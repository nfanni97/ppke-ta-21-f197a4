package com.epam.university.pagemodels.widgets;

import java.time.Duration;

import com.epam.university.pagemodels.pages.BasePage;
import com.epam.university.pagemodels.pages.SearchBookResultsPage;

import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

public class SearchWidget extends BasePage {
    private By searchField = By.id("sitesearch_field");
    private By searchButton = By.cssSelector("#headerSearchForm > a > img");

    public SearchWidget(WebDriver driver) {
        super(driver);
    }

    public void searchForBook(String text) {
        WebElement searchElement = driver.findElement(searchField);
        searchElement.clear();
        searchElement.sendKeys(text);
    }

    public SearchBookResultsPage clickOnSearchButton() {
       driver.findElement(searchButton).click();
       return new SearchBookResultsPage(driver); 
    }
}
