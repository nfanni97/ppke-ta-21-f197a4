package com.epam.university.pagemodels.pages;

import java.time.Duration;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.ElementClickInterceptedException;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.FluentWait;

public class SearchBookResultsPage extends BasePage {
    private By resultTitles = By.cssSelector(".bookTitle > span");
    private By resultLinks = By.className("bookTitle");
    private By loginPromptClose = By.cssSelector("body > div:nth-child(3) > div > div > div.modal__close > button");

    public SearchBookResultsPage(WebDriver driver) {
        super(driver);

    }

    public List<WebElement> getResultLinks() {
        List<WebElement> links = driver.findElements(resultTitles);
        closeLoginPrompt();
        return links;
    }

    public AuthorPage goToAuthorPage(String author) {
        //find link with author name
        WebElement authorLink = driver.findElement(By.linkText(author));
        closeLoginPrompt();
        // go to new page
        authorLink.click();
        return new AuthorPage(driver);
    }

    private void closeLoginPrompt() {
        WebElement toClose = driver.findElement(loginPromptClose);
        new FluentWait<WebDriver>(driver)
            .withTimeout(Duration.ofSeconds(5))
            .pollingEvery(Duration.ofSeconds(1))
            .ignoring(ElementClickInterceptedException.class).until(ExpectedConditions.elementToBeClickable(toClose));
        toClose.click();
    }

    /***
     * 
     * @param n: 1-based indexing of results
     * @return
     */
    public BookPage goToNthResult(int n) throws ArrayIndexOutOfBoundsException {
        List<WebElement> results = driver.findElements(resultLinks);
        if(n>results.size() || n < 1) {
            throw new ArrayIndexOutOfBoundsException();
        }
        WebElement toClick = results.get(n-1);
        // close login prompt
        closeLoginPrompt();
        toClick.click();
        return new BookPage(driver);
    }
}
