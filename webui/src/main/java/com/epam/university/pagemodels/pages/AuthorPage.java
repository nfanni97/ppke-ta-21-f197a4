package com.epam.university.pagemodels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class AuthorPage extends BasePage {
    private By booksLink = By.partialLinkText("distinct work");

    public AuthorPage(WebDriver driver) {
        super(driver);
    }

    public Integer getBooksNumber() {
        return Integer.parseInt(driver.findElement(booksLink).getText().split(" ")[0]);
    }
}
