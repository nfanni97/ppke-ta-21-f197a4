package com.epam.university.pagemodels.pages;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class BookPage extends BasePage {
    private By authorField = By.className("authorName");

    public BookPage(WebDriver driver) {
        super(driver);
    }

    public String getAuthorName() {
        return driver.findElement(authorField).getText();
    }
}
