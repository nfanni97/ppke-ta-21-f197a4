import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertThat;

import java.util.List;
import java.util.stream.Collectors;

import com.epam.university.pagemodels.pages.AuthorPage;
import com.epam.university.pagemodels.pages.BookPage;
import com.epam.university.pagemodels.pages.SearchBookResultsPage;
import com.epam.university.pagemodels.widgets.SearchWidget;

import static org.hamcrest.CoreMatchers.containsString;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

import junitparams.FileParameters;
import junitparams.JUnitParamsRunner;

@RunWith(JUnitParamsRunner.class)
public class BeadandoPageObjectTest extends TestBase{

    @Test
    @FileParameters("src/main/resources/tdd/searchTermContain.csv")
    public void testSearchTermContainedInTitle(String searchTerm) {
        final String searchTermLower = searchTerm.toLowerCase();
        //search for term
        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.navigateToGoodreads(driver);
        searchWidget.searchForBook(searchTermLower);
        SearchBookResultsPage results = searchWidget.clickOnSearchButton();
        //get result titles on first page
        List<String> titles = results.getResultLinks().stream().map(element -> element.getAttribute("innerHTML")).collect(Collectors.toList());
        titles.forEach(title -> assertThat("The title should contain the search term.", title.toLowerCase(), containsString(searchTermLower)));
    }

    @Test
    @FileParameters("src/main/resources/tdd/authorOfBook.csv")
    public void testAuthorOfBook(String bookTitle, String authorExpected) {
        // search for term
        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.navigateToGoodreads(driver);
        searchWidget.searchForBook(bookTitle);
        SearchBookResultsPage results = searchWidget.clickOnSearchButton();
        //nanvigate to first result page
        BookPage book = results.goToNthResult(1);
        assertEquals(authorExpected, book.getAuthorName().toLowerCase());
    }
    
    @Test
    @FileParameters("src/main/resources/tdd/authorNumBooks.csv")
    public void testAuthorNumBooks(String bookTitle, String author, Integer numBooksExpected) {
        //search for term
        SearchWidget searchWidget = new SearchWidget(driver);
        searchWidget.navigateToGoodreads(driver);
        searchWidget.searchForBook(bookTitle);
        SearchBookResultsPage results = searchWidget.clickOnSearchButton();
        //navigate to author page
        AuthorPage authorPage = results.goToAuthorPage(author);
        assertEquals(numBooksExpected, authorPage.getBooksNumber());
    }
}
