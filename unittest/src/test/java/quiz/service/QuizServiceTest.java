package quiz.service;

import quiz.domain.Category;
import quiz.domain.Quiz;
import quiz.domain.Question;
import quiz.repository.QuizRepository;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import static org.junit.jupiter.api.Assertions.*;
import static org.mockito.Mockito.*;

import java.util.Arrays;
import java.util.List;
import java.util.NoSuchElementException;

@ExtendWith(MockitoExtension.class)
public class QuizServiceTest {

    private static final Integer dummyQuizId = 42;
    private static final Integer wrongQuizId = 24601;
    private static Quiz dummyQuiz;
    private static @Mock QuizRepository mockRepository;
    private static QuizService quizService;

    @BeforeAll
    private static void createMockRepositoryAndService() {
        createDummyQuiz();
        mockRepository = mock(QuizRepository.class);
        when(mockRepository.find(Category.History)).thenReturn(
            Arrays.asList(dummyQuiz)
            );
        when(mockRepository.findById(dummyQuizId.toString())).thenReturn(
            dummyQuiz
        );
        quizService = new QuizService(mockRepository);
    }

    private static void createDummyQuiz() {
        dummyQuiz = new Quiz();
        dummyQuiz.setId(dummyQuizId);
        dummyQuiz.setTitle("Murica");
        dummyQuiz.setCategory(Category.Travel);

        Question q1 = new Question();
        q1.setQuestionText("What color is not in the flag?");
        q1.addAnswer("blue");
        q1.addAnswer("yellow");
        q1.addAnswer("white");
        q1.setCorrectAnswerIndex(1);

        Question q2 = new Question();
        q2.setQuestionText("When is Independence Day (dd/mm)?");
        q2.addAnswer("5/12");
        q2.addAnswer("12/6");
        q2.addAnswer("4/7");
        q2.setCorrectAnswerIndex(2);

        dummyQuiz.addQuestion(q1);
        dummyQuiz.addQuestion(q2);
    }

    @Test
    @DisplayName("If every question is correct, the result is 100.")
    public void testCalculateResult_perfectScore() {
        int[] userChoice = {1,2};

        int result = quizService.calculateResult(dummyQuizId.toString(),userChoice);

        assertEquals(100,result,"For correct answers, result should be 100.");
    }

    @Test
    @DisplayName("If half of the questions are correct, the result is 50.")
    public void testCalculateResult_halfScore() {
        int[] userChoice = {3,2};

        int result = quizService.calculateResult(dummyQuizId.toString(),userChoice);

        assertEquals(50,result,"For half-correct answers, result should be 50.");
    }

    @Test
    @DisplayName("If quiz is not found, throws exception")
    public void testCalculateResult_notFoundException() {
        int[] userChoice = {3,2};

        //interaction: in assertion

        assertThrows(
            NoSuchElementException.class,
            () -> quizService.calculateResult(wrongQuizId.toString(),userChoice),
            "Should throw NoSuchElementException for not found quiz.");
    }

    @Test
    @DisplayName("There has to be as many given answers as questions.")
    public void testCalculateResult_answerNumber() {
        //dummyQuiz has 2 questions
        int[] userChoiceTooFew = {1};
        int[] userChoiceTooMany = {3,4,51};

        //interaction: in assertion

        assertAll(
            "Reaction for different number of questions and answers",
            () -> assertThrows(
                IllegalArgumentException.class,
                () -> quizService.calculateResult(dummyQuizId.toString(),userChoiceTooFew),
                "Should throw IllegalArgumentException if too few answers."
            ),
            () -> assertThrows(
                IllegalArgumentException.class,
                () -> quizService.calculateResult(dummyQuizId.toString(),userChoiceTooMany),
                "Should throw IllegalArgumentException if too many answers."
            )
        );
    }

    @Test
    @DisplayName("The index of the correct answer is erased from the Question objects.")
    public void testFind_correctAnswerIsErased() {
        //setup: done in beforeAll

        List<Quiz> foundQuizzes = quizService.find(Category.History);
        
        for(Quiz quiz: foundQuizzes) {
            for(Question question: quiz.getQuestions()) {
                assertNull(question.getCorrectAnswerIndex(),"The correct answer should be erased from question with text ."+question.getQuestionText());
            }
        }
    }
}
