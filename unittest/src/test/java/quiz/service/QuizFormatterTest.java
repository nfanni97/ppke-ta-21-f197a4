package quiz.service;

import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;

import quiz.domain.Category;
import quiz.domain.Question;
import quiz.domain.Quiz;

public class QuizFormatterTest {

    private final String INDENTATION = "    ";//copied from 
    
    private Quiz createQuiz() {
        Quiz dummyQuiz = new Quiz();
        dummyQuiz.setId(42);
        dummyQuiz.setTitle("Murica");
        dummyQuiz.setCategory(Category.Travel);

        Question q1 = new Question();
        q1.setQuestionText("What color is not in the flag?");
        q1.addAnswer("blue");
        q1.addAnswer("yellow");
        q1.addAnswer("white");
        q1.setCorrectAnswerIndex(1);

        Question q2 = new Question();
        q2.setQuestionText("When is Independence Day (dd/mm)?");
        q2.addAnswer("5/12");
        q2.addAnswer("12/6");
        q2.addAnswer("4/7");
        q2.setCorrectAnswerIndex(2);

        dummyQuiz.addQuestion(q1);
        dummyQuiz.addQuestion(q2);

        return dummyQuiz;
    }

    private String createQuizString(Quiz quiz) {
        return "Quiz id: 42, category: Travel, title: 'Murica'" + System.lineSeparator()
            + INDENTATION + "Question: 'What color is not in the flag?'" + System.lineSeparator()
            + INDENTATION + INDENTATION + "1. blue" + System.lineSeparator()
            + INDENTATION + INDENTATION + "2. yellow" + System.lineSeparator()
            + INDENTATION + INDENTATION + "3. white" + System.lineSeparator()
            + INDENTATION + "Question: 'When is Independence Day (dd/mm)?'" + System.lineSeparator()
            + INDENTATION + INDENTATION + "1. 5/12" + System.lineSeparator()
            + INDENTATION + INDENTATION + "2. 12/6" + System.lineSeparator()
            + INDENTATION + INDENTATION + "3. 4/7" + System.lineSeparator();
    }

    @Test
    @DisplayName("Formatting of a quiz.")
    void testFormatQuizAndQuestions() {
        Quiz quiz = createQuiz();
        String expected = createQuizString(quiz);

        String calculated = QuizFormatter.formatQuizAndQuestions(quiz);

        assertEquals(expected, calculated, "The calculated text is not the same as the expected text.");
    }
}
