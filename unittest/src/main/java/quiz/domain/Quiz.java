package quiz.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Quiz {
    private int id;
    private Category category;
    private String title;

    private List<Question> questions = new ArrayList<>();

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Category getCategory() {
        return category;
    }

    public void setCategory(Category category) {
        this.category = category;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public void addQuestion(Question question) {
        questions.add(question);
    }

    public List<Question> getQuestions() {
        return Collections.unmodifiableList(questions);
    }

    public Quiz copyWithCorrectAnswerErased() {
        Quiz quizCopy = new Quiz();
        quizCopy.setId(id);
        quizCopy.setCategory(category);
        quizCopy.setTitle(title);
        for (Question question : questions) {
            quizCopy.addQuestion(question.copyWithCorrectAnswerErased());
        }
        return quizCopy;
    }
}
