package quiz.domain;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Question implements Cloneable {
    private String questionText;
    private List<String> answers = new ArrayList<>();
    private Integer correctAnswerIndex;

    public void addAnswer(String answer) {
        answers.add(answer);
    }

    public String getQuestionText() {
        return questionText;
    }

    public void setQuestionText(String questionText) {
        this.questionText = questionText;
    }

    public Integer getCorrectAnswerIndex() {
        return correctAnswerIndex;
    }

    public void setCorrectAnswerIndex(Integer correctAnswerIndex) {
        this.correctAnswerIndex = correctAnswerIndex;
    }

    public List<String> getAnswers() {
        return Collections.unmodifiableList(answers);
    }

    public Question copyWithCorrectAnswerErased() {
        Question copy = null;
        try {
            copy = (Question) this.clone();
        } catch (CloneNotSupportedException e) {
            e.printStackTrace();
        }
        if (copy != null) {
            copy.setCorrectAnswerIndex(null);
        }
        return copy;
    }

    @Override
    public Object clone() throws CloneNotSupportedException {
        return (Question) super.clone();
    }
}
