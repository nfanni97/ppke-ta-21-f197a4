package quiz.service;

import quiz.domain.Question;
import quiz.domain.Quiz;

import java.util.List;

public class QuizFormatter {
    private static final String INDENTATION = "    ";

    public static String formatQuizAndQuestions(Quiz quiz) {
        StringBuilder builder = new StringBuilder();
        builder.append(formatQuizOnly(quiz));
        builder.append(System.lineSeparator());
        for (Question question : quiz.getQuestions()) {
            builder.append(formatQuestionAndAnswers(question));
        }
        return builder.toString();
    }

    public static String formatQuizOnly(Quiz quiz) {
        return String.format("Quiz id: %d, category: %s, title: '%s'", quiz.getId(), quiz.getCategory(), quiz.getTitle());
    }

    private static String formatQuestionAndAnswers(Question question)  {
        StringBuilder builder = new StringBuilder();
        builder.append(formatQuestionOnly(question));
        builder.append(System.lineSeparator());
        List<String> answers = question.getAnswers();
        for (int i = 0; i < answers.size(); i++) {
            builder.append(formatAnswer(answers, i));
            builder.append(System.lineSeparator());
        }
        return builder.toString();
    }

    private static String formatAnswer(List<String> answers, int i) {
        return String.format("%s%s%d. %s", INDENTATION, INDENTATION, i + 1, answers.get(i));
    }

    private static String formatQuestionOnly(Question question) {
        return String.format("%sQuestion: '%s'", INDENTATION, question.getQuestionText());
    }
}
