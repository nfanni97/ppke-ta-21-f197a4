package quiz.service;

import quiz.domain.Category;
import quiz.domain.Question;
import quiz.domain.Quiz;
import quiz.repository.QuizRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.NoSuchElementException;

public class QuizService {

    private final QuizRepository quizRepository;

    public QuizService(QuizRepository quizRepository) {
        this.quizRepository = quizRepository;
    }

    /**
     * Search quizzes by catgeory
     * @param category quiz category to search quiz for
     * @return copy of quizzes that has the given category and correctAnswerIndex field is erased in each question.
     */
    public List<Quiz> find(Category category) {
        List<Quiz> result = quizRepository.find(category);
        List<Quiz> quizzesClone = new ArrayList<>();
        for (Quiz quiz : result) {
            quizzesClone.add(quiz.copyWithCorrectAnswerErased());
        }
        return quizzesClone;
    }

    /**
     * Calculates quiz result based on user choice.
     * @param quizId id of the quiz that the user answers
     * @param userChoice user choice for the questions; number of items must match the number of questions in the quiz
     * @return result in percent, e.g. 75 if user gave 3 correct answers in a 4 questions quiz
     * @exception java.util.NoSuchElementException if quiz not found by id
     * @exception IllegalArgumentException userChoice size is not the same as number of questions in the quiz
     */
    public int calculateResult(String quizId, int[] userChoice)  {
        if(quizId == null || userChoice == null) {
            throw new IllegalArgumentException("Null parameter not allowed!");
        }
        Quiz quiz = quizRepository.findById(quizId);
        if(quiz == null) {
            throw new NoSuchElementException("Quiz not found by id.");
        }
        if(quiz.getQuestions().size() != userChoice.length) {
            throw new IllegalArgumentException("The number of answers are not correct.");
        }
        return calculate(userChoice, quiz);
    }

    private int calculate(int[] userChoice, Quiz quiz) {
        int correctAnswers = 0;
        int indexOfUserChoice = 0;
        for (Question question: quiz.getQuestions()) {
            if(question.getCorrectAnswerIndex().equals(userChoice[indexOfUserChoice])) {
                correctAnswers++;
            }
            indexOfUserChoice++;
        }
        return (int) (((correctAnswers * 1.0) / userChoice.length) * 100);
    }

}
