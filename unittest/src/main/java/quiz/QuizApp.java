package quiz;

import quiz.domain.Category;
import quiz.domain.Quiz;
import quiz.repository.InMemoryQuizRepository;
import quiz.service.QuizFormatter;
import quiz.service.QuizService;

import java.util.List;
import java.util.Scanner;
import java.util.stream.Collectors;

public class QuizApp {
    private static Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        QuizService quizService = new QuizService(new InMemoryQuizRepository());

        Category category = chooseCategory(scanner);
        List<Quiz> quizzes = quizService.find(category);
        printQuizzes(quizzes);
        System.out.print("Which quiz would you like to take? (type id): ");
        String quizId = scanner.nextLine();

        Quiz quiz = quizzes.stream()
                .filter(qz -> qz.getId() == Integer.parseInt(quizId))
                .collect(Collectors.toList())
                .get(0);
        System.out.println(QuizFormatter.formatQuizAndQuestions(quiz));
        int[] answers = getAnswers(scanner);
        int percentage = quizService.calculateResult(quizId, answers);
        System.out.println("Your result is " + percentage + " %");
    }

    private static int[] getAnswers(Scanner scanner) {
        System.out.print("Your answers (type numbers one for each question): ");
        String answers = scanner.nextLine();
        return prepareAnswers(answers);
    }

    private static int[] prepareAnswers(String answers) {
        int[] preparedAnswers = new int[answers.length()];
        for(int i = 0; i < answers.length(); i++){
            preparedAnswers[i] = Integer.parseInt("" + answers.charAt(i)) - 1;
        }
        return preparedAnswers;
    }

    private static void printQuizzes(List<Quiz> quizzes) {
        System.out.println("List of quizzes in the chosen category: ");
        for (Quiz quiz : quizzes) {
            System.out.println(QuizFormatter.formatQuizOnly(quiz));
        }
    }

    private static Category chooseCategory(Scanner scanner) {
        System.out.println("Available categories:");
        for (Category cat: Category.values()) {
            System.out.println("    " + cat);
        }
        System.out.print("Please select (type category name): ");
        String categoryChoice = scanner.nextLine();
        return Category.valueOf(categoryChoice);
    }
}
