package quiz.repository;

import quiz.domain.Category;
import quiz.domain.Question;
import quiz.domain.Quiz;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class InMemoryQuizRepository implements QuizRepository {

    public List<Quiz> quizzes = new ArrayList<>();

    public InMemoryQuizRepository() {
        addHistoryQuiz();
        addTravelQuiz();
    }

    private void addHistoryQuiz() {
        Quiz quiz = new Quiz();
        quiz.setId(1);
        quiz.setTitle("Mixed easy history questions");
        quiz.setCategory(Category.History);

        Question question1 = new Question();
        question1.setQuestionText("Alexander the great was the leader of...");
        question1.addAnswer("Roman");
        question1.addAnswer("Greek");
        question1.addAnswer("German");
        question1.addAnswer("Persian");
        question1.setCorrectAnswerIndex(1);

        quiz.addQuestion(question1);

        Question question2 = new Question();
        question2.setQuestionText("Battle at Nandorfehervar when Hungarian forces won over Turkish happened in which year?");
        question2.addAnswer("1222");
        question2.addAnswer("1526");
        question2.addAnswer("1456");
        question2.addAnswer("1848");
        question2.setCorrectAnswerIndex(2);

        quiz.addQuestion(question2);

        quizzes.add(quiz);
    }

    private void addTravelQuiz() {
        Quiz quiz = new Quiz();
        quiz.setId(2);
        quiz.setCategory(Category.Travel);
        quiz.setTitle("Questions about travel :) ");
        Question question1 = new Question();
        question1.setQuestionText("Eiffel tower is made of what material?");
        question1.addAnswer("Stone");
        question1.addAnswer("Wooden");
        question1.addAnswer("Metal");
        question1.setCorrectAnswerIndex(2);

        quiz.addQuestion(question1);

        Question question2 = new Question();
        question2.setQuestionText("Sagrada Familia church in Barcelona was designed by who?");
        question2.addAnswer("Miklos Ybl");
        question2.addAnswer("Antoni Gaudi");
        question2.addAnswer("Ödön Lechner");
        question2.setCorrectAnswerIndex(1);

        quiz.addQuestion(question2);

        quizzes.add(quiz);
    }

    @Override
    public List<Quiz> find(Category category) {
        return quizzes.stream()
                .filter(quiz -> category == quiz.getCategory())
                .collect(Collectors.toList());
    }

    @Override
    public Quiz findById(String id) {
        Quiz quizToReturn = null;
        List<Quiz> result = new ArrayList<>();
        for (Quiz quiz: quizzes) {
            if(quiz.getId() == Integer.parseInt(id)) {
                result.add(quiz);
            }
        }
        if(result.size() != 0) {
            quizToReturn = result.get(0);
        }
        return quizToReturn;
    }
}
