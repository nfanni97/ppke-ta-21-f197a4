package quiz.repository;

import quiz.domain.Category;
import quiz.domain.Quiz;

import java.util.List;

public interface QuizRepository {
    /**
     * Search quizes by category
     * @param category quiz category to search quiz for
     * @return quizes that has the given category
     */
    List<Quiz> find(Category category);

    /**
     * Find quiz by id.
     * @param id id of the quiz
     * @return quiz that has the given id
     */
    Quiz findById(String id);
}
