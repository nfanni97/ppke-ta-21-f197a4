# Quiz application

Users can test their knowledge with the Quiz Application.
The application has quizzes for different categories like history, travel.
Users can choose a category and select a quiz from the available quizzes of the given category.
Based on the user choice for each question, the application evaluates the result and print it as a percentage.

You can test the application by running QuizApp.


# Unit Testing task

## Unit test for QuizService

Create a mock object for QuizRepository and inject it into QuizService.
Train the mock object to give result for the find() and findById() method calls.

- Test QuizService.find() method.
  - Make sure that correctAnswer is erased.
- Test QuizService.calculateResult()
  - Test calculation when the user provides correct answer for all  questions. (result is 100)
  - Test calculation when the user provides correct answer for half of the questions. (result is 50)
  - Test that the quiz is not found in QuizRepository with the given quizId.
    Check that the service throws NoSuchElementException.
  - Test that the number of elements in userChoice is different than the number of questions in the quiz.
    Check that the service throws IllegalArgumentException.

## Unit test for QuizFormatter

- Test QuizFormatter.formatQuizAndQuestions()
  - Provide input that has at least two questions, at least two answers for each question.
